/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
'use strict';

// [START all]
// [START import]
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const sld_rpc = require('node-bitcoin-rpc');
const functions = require('firebase-functions');
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const corsHandler = require('cors')({ origin: true });
admin.initializeApp();
// [END import]
function requestPayment(url, success) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	xhr.onreadystatechange = function() {
		if (xhr.readyState > 3 && xhr.status === 200) {
			success(xhr.responseText);
		}
	};
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send();
	return xhr;
}
function rpcRequest(url, success) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url);
	xhr.onreadystatechange = function() {
		if (xhr.readyState > 3 && xhr.status === 200) {
			success(xhr.responseText);
		}
	};
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.setRequestHeader(
		'Data',
		'{"jsonrpc": "1.0", "id":"curltest", "method": "getinfo", "params": [] }'
	);

	xhr.send();
	return xhr;
}
exports.getRPCInfo = functions.https.onRequest((req, res) => {
	corsHandler(req, res, () => {
		sld_rpc.init('35.237.208.27', 3999, 'darius', 'rucker');
		sld_rpc.call('getbalance', [], function(err, res) {
			if (err !== null) {
				console.log('I have an error :( ' + err + ' ' + res);
			} else {
				console.log('Yay! I need to do whatevere now with ' + res.result);
			}
			return res.status(200).send('sucess');
		});
	});
});
// [START addMessage]
// Take the text parameter passed to this HTTP endpoint and insert it into the
// Realtime Database under the path /messages/:pushId/original
// [START addMessageTrigger]
exports.addMessage = functions.https.onRequest((req, res) => {
	// [END addMessageTrigger]
	// Grab the text parameter.
	// https://us-central1-sollida-39b00.cloudfunctions.net/addMessage?uid=e7as7d68as&email=teste@live.com&order_id=823&price_amount=100
	corsHandler(req, res, () => {
		const uid = req.query.uid,
			email = req.query.email,
			order_id = req.query.order_id,
			price_amount = req.query.price_amount;
		if (!email | !order_id | !price_amount) return null;
		// [START adminSdkPush]
		// Push the new message into the Realtime Database using the Firebase Admin SDK.
		return requestPayment(
			`http://35.231.90.98/coingate/create?order_id=${uid}&price_amount=${price_amount}`,
			data => {
				const obj = JSON.parse(data);
				return admin
					.database()
					.ref('/order/' + uid + '/' + obj[0].id)
					.set({
						created_at: obj[0].created_at,
						id: obj[0].id,
						order_id: obj[0].order_id,
						payment_url: obj[0].payment_url,
						price_amount: obj[0].price_amount,
						price_currency: obj[0].price_currency,
						receive_amount: obj[0].receive_amount,
						receive_currency: obj[0].receive_currency,
						status: obj[0].status,
						userUID: uid
					})
					.then(snapshot => {
						// Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
						return res.status(200).send('sucess');
					});
			}
		);
	});
});

exports.newsLetter = functions.https.onRequest((req, res) => {
	corsHandler(req, res, () => {
		let email = req.query.email;

		return requestPayment(
			`http://35.231.90.98/mailchimp/news?email=${email}`,
			data => {
				const obj = JSON.parse(data);
				return res.status(200).send(obj);
			}
		);
	});
});

exports.reqTelegram = functions.https.onRequest((req, res) => {
	corsHandler(req, res, () => {
		/*const text = {
			update_id: 288256890,
			message: {
				message_id: 18,
				from: {
					id: 409665429,
					is_bot: false,
					first_name: 'Diel',
					username: 'diel_zito',
					language_code: 'pt-BR'
				},
				chat: {
					id: -275687807,
					title: 'SollidaCoin',
					type: 'group',
					all_members_are_administrators: false
				},
				date: 1533824547,
				text: 'Get Sollida Coins!'
			}
		};*/
		let query = req.body;

		admin
			.database()
			.ref('/AirDrop/telegram/')
			.on('value', snapshot => {
				let i = 0;
				const data = snapshot.val();
				for (let prop in data) {
					for (let prop in data) {
						if (prop === query.message.text) i = 1;
					}
					for (let prop in data) {
						if (data[prop].userID === query.message.from.id) {
							console.log('Ja existe esse usuario');
							return;
						}
					}
					if (i < 1) {
						console.log('nao achou UID');
						return;
					}
					if (prop === query.message.text) {
						requestPayment(
							`https://api.telegram.org/bot613114312:AAFhtDLoDXjpxcWfY30qa-nNhIx4HDrmikM/sendMessage?chat_id=-275687807&text=${
								query.message.from.first_name
							}%20obtained%20500%20SLD.`,
							data => {
								const obj = JSON.parse(data);
								admin
									.database()
									.ref('/AirDrop/telegram/' + query.message.text)
									.update({
										active: true,
										userID: query.message.from.id,
										tokens: 500
									});
							}
						);
					}
				}
			});

		console.log('kkk foi');
	});
	return res.status(200).send('sucess');
});

exports.getTotalTokensSld = functions.https.onRequest((req, res) => {
	corsHandler(req, res, () => {
		let _valu = 0;
		return admin
			.database()
			.ref('/SLD/')
			.on('value', snapshot => {
				let i = 0;
				let a = 'sucess';
				const data = snapshot.val();
				for (let prop in data) {
					const fatherProp = data[prop];
					for (let props in fatherProp) {
						i += fatherProp[props].tokens;
					}
				}
				return res.status(200).send(i.toString());
			});
	});
});

exports.getTotalMoney = functions.https.onRequest((req, res) => {
	corsHandler(req, res, () => {
		let _valu = 0;
		return admin
			.database()
			.ref('/SLD/')
			.on('value', snapshot => {
				let i = 0;
				const data = snapshot.val();
				for (let prop in data) {
					const fatherProp = data[prop];
					for (let props in fatherProp) {
						i += fatherProp[props].value;
					}
				}
				return res.status(200).send(i.toString());
			});
	});
});

exports.callBackStatus = functions.https.onRequest((req, res) => {
	corsHandler(req, res, () => {
		const query = req;
		if (query.body.status !== 'paid') return null;
		//console.log(query.body);
		if (!query.body.order_id | !query.body.created_at | !query.body.id) {
			return null;
		}

		return admin
			.database()
			.ref('/SLD/' + query.body.order_id + '/' + query.body.id)
			.set({
				tokens: query.body.price_amount * 100 * 2, //update bonus here
				value: query.body.price_amount,
				newOrder: true
			})
			.then(
				admin
					.database()
					.ref('/order/' + query.body.order_id + '/' + query.body.id)
					.update({
						pay_currency: query.body.pay_currency,
						price_amount: query.body.price_amount,
						receive_amount: query.body.receive_amount,
						status: query.body.status
					})
					.then(snapshot => {
						// Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
						return res.status(200).send('sucess');
					})
			);
	});
});

// [END addMessage]

// [START makeUppercase]
// Listens for new messages added to /messages/:pushId/original and creates an
// uppercase version of the message to /messages/:pushId/uppercase
exports.createTokens = functions.database
	.ref('/profile/{uid}')
	.onCreate((snapshot, context) => {
		// Grab the current value of what was written to the Realtime Database.
		const content = snapshot.val();
		return admin
			.database()
			.ref('/SLD/' + snapshot.val().uid + '/' + snapshot.val.id)
			.set({
				tokens: 0,
				value: 0,
				userUID: snapshot.val().uid,
				newOrder: false
			})
			.then(snapshot => {
				// Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
				return admin
					.database()
					.ref('/AirDrop/telegram/' + content.uid)
					.set({
						active: false,
						userID: 0
					});
			});
	});

exports.changeStatus = functions.database
	.ref('/profile/{uid}')
	.onWrite((change, context) => {
		const post = change.after.val();

		if (post.CloudF) {
			// prevent infinite looping
			return null;
		}
		return change.before.ref.update({
			CloudF: true
		});
		//return change.before.data.ref.set(post);
		//console.log(post);
	});

//interceptar e somar
exports.recalculateTokens = functions.database
	.ref('/SLD/{uid}')
	.onWrite((change, context) => {
		const post = change.after.val();
		const beforepost = change.before.val();

		if (change.after.val().lastCheck + 1000 > Date.now()) return null;
		if (!change.after.val().newOrder) return null;
		return change.before.ref.update({
			//tokens: change.after.val().tokens + change.before.val().tokens,
			lastCheck: Date.now()
			//newOrder: false
		});
		//return change.before.data.ref.set(post);
		//console.log(post);
	});
// [END all]
